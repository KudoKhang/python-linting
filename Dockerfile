FROM python:3.9-alpine

ENV SHELL /bin/bash

WORKDIR /app

COPY requirements.txt /app

RUN pip install --no-cache-dir -r requirements.txt

